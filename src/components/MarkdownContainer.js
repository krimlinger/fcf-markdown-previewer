import React, { useEffect, useState } from 'react';
import { Container, Col, Row } from '../BootstrapGrid';
import marked from 'marked';
import DOMPurify from 'dompurify';
import './MarkdownContainer.scss';
import loremMarkdownum from '../loremMarkdownum';

function markdownize(str) {
  return DOMPurify.sanitize(marked(str));
}

export default function MarkdownContainer({
  reset = false,
  showRawHTML,
  toggleView}) {
  const [markdown, setMarkdown] = useState(loremMarkdownum);

  useEffect(() => {
    setMarkdown('');
  }, [reset]);

  useEffect(() => setMarkdown(loremMarkdownum), []);

  return (
    <Container
      as="main"
      fluid
      className="d-flex flex-column flex-fill p-3"
    >
      <Row className={"text-center" + (toggleView ? " d-none": "")}>
        <Col>
          Mardown
        </Col>
        <Col>
          Preview
        </Col>
      </Row>
      <Row className={"flex-fill" + (toggleView ? " flex-column": "")}>
        <Col id="editor-col" className="mb-2">
          <textarea
            id="editor"
            className="border border-secondary h-100 w-100 p-3"
            onChange={(ev) => setMarkdown(ev.target.value)}
            value={markdown}
          >
          </textarea>
        </Col>
        <Col id="preview-col" className="mb-2">
          {!showRawHTML ? (
            <div
              id="preview"
              className="border border-primary h-100 w-100 p-3"
              dangerouslySetInnerHTML={{__html: markdownize(markdown)}}
            >
            </div>
          ): (
            <textarea
              id="preview"
              className="border border-primary h-100 w-100 p-3"
              value={markdownize(markdown)}
              readOnly
            >
            </textarea>
          )}
        </Col>
      </Row>
    </Container>
  );
}
