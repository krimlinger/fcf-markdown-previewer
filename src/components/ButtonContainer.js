import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Container, Col, Row } from '../BootstrapGrid';

export default function ButtonContainer({
  onResetClick,
  onToggleShowHTML,
  onToggleView,
  showRawHTML,
  toggleView}) {
  const [showModal, setShowModal] = useState(false);

  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);
  const handleCloseModalReset = () => {
    handleCloseModal();
    onResetClick();
  };

  return (
    <>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Reset confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to reset the markdown ?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="danger" onClick={handleCloseModalReset}>
            Yes, reset !
          </Button>
        </Modal.Footer>
      </Modal>
      <Container fluid>
        <Row
          noGutters
          className="justify-content-end p-2"
        >
          <Col xs={12} sm="auto" className="m-1">
            <Button
              block
              variant="danger"
              onClick={handleShowModal}
            >
              Reset Markdown
            </Button>
          </Col>
          <Col xs={12} sm="auto" className="m-1">
            <Button
              block
              variant={toggleView ? "primary": "secondary"}
              onClick={onToggleView}
            >
              Toggle view
            </Button>
          </Col>
          <Col xs={12} sm="auto" className="m-1">
            <Button
              block
              variant={showRawHTML ? "secondary": "primary"}
              onClick={onToggleShowHTML}
            >
              Toggle raw HTML
            </Button>
          </Col>
        </Row>
      </Container>
    </>
  );
}
