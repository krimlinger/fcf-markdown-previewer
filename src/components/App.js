import React, { useState } from 'react';
import './App.scss';
import ButtonContainer from './ButtonContainer';
import MarkdownContainer from './MarkdownContainer';

export default function App() {
  const [reset, setReset] = useState(false);
  const [showRawHTML, setShowRawHTML] = useState(false);
  const [toggleView, setToggleView] = useState(false);

  // Update MarkdownContainer by changing its prop.
  const onResetClick = () => {
    setReset(!reset);
  };

  const onToggleShowHTML = () => {
    setShowRawHTML(!showRawHTML);
  }

  const onToggleView = () => {
    setToggleView(!toggleView);
  }

  return (
    <>
      <ButtonContainer
        onResetClick={onResetClick}
        onToggleShowHTML={onToggleShowHTML}
        showRawHTML={showRawHTML}
        onToggleView={onToggleView}
        toggleView={toggleView}
      />
      <MarkdownContainer
        reset={reset}
        showRawHTML={showRawHTML}
        toggleView={toggleView}
      />
    </>
  );
}
